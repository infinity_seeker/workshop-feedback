defmodule WorkshopFeedback.StudentcommentTest do
  use WorkshopFeedback.DataCase

  alias WorkshopFeedback.Studentcomment

  describe "feedbacks" do
    alias WorkshopFeedback.Studentcomment.Feedback

    @valid_attrs %{first_day_feedback: "some first_day_feedback", first_session_rating: 42, future_suggestion: "some future_suggestion", language_preference: "some language_preference", second_session_feedback: "some second_session_feedback", second_session_rating: 42}
    @update_attrs %{first_day_feedback: "some updated first_day_feedback", first_session_rating: 43, future_suggestion: "some updated future_suggestion", language_preference: "some updated language_preference", second_session_feedback: "some updated second_session_feedback", second_session_rating: 43}
    @invalid_attrs %{first_day_feedback: nil, first_session_rating: nil, future_suggestion: nil, language_preference: nil, second_session_feedback: nil, second_session_rating: nil}

    def feedback_fixture(attrs \\ %{}) do
      {:ok, feedback} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Studentcomment.create_feedback()

      feedback
    end

    test "list_feedbacks/0 returns all feedbacks" do
      feedback = feedback_fixture()
      assert Studentcomment.list_feedbacks() == [feedback]
    end

    test "get_feedback!/1 returns the feedback with given id" do
      feedback = feedback_fixture()
      assert Studentcomment.get_feedback!(feedback.id) == feedback
    end

    test "create_feedback/1 with valid data creates a feedback" do
      assert {:ok, %Feedback{} = feedback} = Studentcomment.create_feedback(@valid_attrs)
      assert feedback.first_day_feedback == "some first_day_feedback"
      assert feedback.first_session_rating == 42
      assert feedback.future_suggestion == "some future_suggestion"
      assert feedback.language_preference == "some language_preference"
      assert feedback.second_session_feedback == "some second_session_feedback"
      assert feedback.second_session_rating == 42
    end

    test "create_feedback/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Studentcomment.create_feedback(@invalid_attrs)
    end

    test "update_feedback/2 with valid data updates the feedback" do
      feedback = feedback_fixture()
      assert {:ok, %Feedback{} = feedback} = Studentcomment.update_feedback(feedback, @update_attrs)
      assert feedback.first_day_feedback == "some updated first_day_feedback"
      assert feedback.first_session_rating == 43
      assert feedback.future_suggestion == "some updated future_suggestion"
      assert feedback.language_preference == "some updated language_preference"
      assert feedback.second_session_feedback == "some updated second_session_feedback"
      assert feedback.second_session_rating == 43
    end

    test "update_feedback/2 with invalid data returns error changeset" do
      feedback = feedback_fixture()
      assert {:error, %Ecto.Changeset{}} = Studentcomment.update_feedback(feedback, @invalid_attrs)
      assert feedback == Studentcomment.get_feedback!(feedback.id)
    end

    test "delete_feedback/1 deletes the feedback" do
      feedback = feedback_fixture()
      assert {:ok, %Feedback{}} = Studentcomment.delete_feedback(feedback)
      assert_raise Ecto.NoResultsError, fn -> Studentcomment.get_feedback!(feedback.id) end
    end

    test "change_feedback/1 returns a feedback changeset" do
      feedback = feedback_fixture()
      assert %Ecto.Changeset{} = Studentcomment.change_feedback(feedback)
    end
  end
end
