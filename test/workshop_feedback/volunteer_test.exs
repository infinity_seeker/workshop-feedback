defmodule WorkshopFeedback.VolunteerTest do
  use WorkshopFeedback.DataCase

  alias WorkshopFeedback.Volunteer

  describe "members" do
    alias WorkshopFeedback.Volunteer.Member

    @valid_attrs %{college_name: "some college_name", email: "some email", name: "some name", phone_no: "some phone_no"}
    @update_attrs %{college_name: "some updated college_name", email: "some updated email", name: "some updated name", phone_no: "some updated phone_no"}
    @invalid_attrs %{college_name: nil, email: nil, name: nil, phone_no: nil}

    def member_fixture(attrs \\ %{}) do
      {:ok, member} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Volunteer.create_member()

      member
    end

    test "list_members/0 returns all members" do
      member = member_fixture()
      assert Volunteer.list_members() == [member]
    end

    test "get_member!/1 returns the member with given id" do
      member = member_fixture()
      assert Volunteer.get_member!(member.id) == member
    end

    test "create_member/1 with valid data creates a member" do
      assert {:ok, %Member{} = member} = Volunteer.create_member(@valid_attrs)
      assert member.college_name == "some college_name"
      assert member.email == "some email"
      assert member.name == "some name"
      assert member.phone_no == "some phone_no"
    end

    test "create_member/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Volunteer.create_member(@invalid_attrs)
    end

    test "update_member/2 with valid data updates the member" do
      member = member_fixture()
      assert {:ok, %Member{} = member} = Volunteer.update_member(member, @update_attrs)
      assert member.college_name == "some updated college_name"
      assert member.email == "some updated email"
      assert member.name == "some updated name"
      assert member.phone_no == "some updated phone_no"
    end

    test "update_member/2 with invalid data returns error changeset" do
      member = member_fixture()
      assert {:error, %Ecto.Changeset{}} = Volunteer.update_member(member, @invalid_attrs)
      assert member == Volunteer.get_member!(member.id)
    end

    test "delete_member/1 deletes the member" do
      member = member_fixture()
      assert {:ok, %Member{}} = Volunteer.delete_member(member)
      assert_raise Ecto.NoResultsError, fn -> Volunteer.get_member!(member.id) end
    end

    test "change_member/1 returns a member changeset" do
      member = member_fixture()
      assert %Ecto.Changeset{} = Volunteer.change_member(member)
    end
  end
end
