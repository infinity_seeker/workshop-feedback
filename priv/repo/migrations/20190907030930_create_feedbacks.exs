defmodule WorkshopFeedback.Repo.Migrations.CreateFeedbacks do
  use Ecto.Migration

  def change do
    create table(:feedbacks) do
      add :first_session_rating, :integer
      add :first_day_feedback, :text
      add :second_session_rating, :integer
      add :second_session_feedback, :text
      add :language_preference, :string
      add :future_suggestion, :text

      timestamps()
    end

  end
end
