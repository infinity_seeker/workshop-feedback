defmodule WorkshopFeedback.Repo.Migrations.CreateMembers do
  use Ecto.Migration

  def change do
    create table(:members) do
      add :name, :string
      add :college_name, :string
      add :phone_no, :string
      add :email, :string

      timestamps()
    end

  end
end
