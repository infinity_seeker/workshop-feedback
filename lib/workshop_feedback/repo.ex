defmodule WorkshopFeedback.Repo do
  use Ecto.Repo,
    otp_app: :workshop_feedback,
    adapter: Ecto.Adapters.Postgres
end
