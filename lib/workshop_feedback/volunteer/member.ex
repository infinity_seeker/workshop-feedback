defmodule WorkshopFeedback.Volunteer.Member do
  use Ecto.Schema
  import Ecto.Changeset

  schema "members" do
    field :college_name, :string
    field :email, :string
    field :name, :string
    field :phone_no, :string

    timestamps()
  end

  @doc false
  def changeset(member, attrs) do
    member
    |> cast(attrs, [:name, :college_name, :phone_no, :email])
    |> validate_required([:name, :phone_no, :email])
  end
end
