defmodule WorkshopFeedback.Studentcomment.Feedback do
  use Ecto.Schema
  import Ecto.Changeset

  schema "feedbacks" do
    field :first_day_feedback, :string
    field :first_session_rating, :integer
    field :future_suggestion, :string
    field :language_preference, :string
    field :second_session_feedback, :string
    field :second_session_rating, :integer

    timestamps()
  end

  @doc false
  def changeset(feedback, attrs) do
    feedback
    |> cast(attrs, [:first_session_rating, :first_day_feedback, :second_session_rating, :second_session_feedback, :language_preference, :future_suggestion])
    |> validate_required([:first_session_rating, :first_day_feedback, :second_session_rating, :second_session_feedback, :language_preference, :future_suggestion])
  end
end
