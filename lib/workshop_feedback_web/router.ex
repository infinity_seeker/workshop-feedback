defmodule WorkshopFeedbackWeb.Router do
  use WorkshopFeedbackWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug WorkshopFeedback.Auth, repo: WorkshopFeedback.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WorkshopFeedbackWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/login", SessionController, :new
    get "/logout", SessionController, :delete
    post "/login", SessionController, :create 
    resources "/feedbacks", FeedbackController
    resources "/members", MemberController
  end

  # Other scopes may use custom stacks.
  # scope "/api", WorkshopFeedbackWeb do
  #   pipe_through :api
  # end
end
