defmodule WorkshopFeedback.Auth do
	import Plug.Conn
	import Phoenix.Controller

	def init(opts) do
		Keyword.fetch!(opts, :repo)
	end

	def call(conn, _) do
		conn
	end

	def no_feedbacks(conn, _) do
		user_id = get_session(conn, :user_id)
		if user_id do
			conn
		else
			conn
			|> put_flash(:error, "You cannot access that page")
			|> redirect(to: "/")	
		end
		conn
	end
end