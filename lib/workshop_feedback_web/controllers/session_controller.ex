defmodule WorkshopFeedbackWeb.SessionController do
	use WorkshopFeedbackWeb, :controller

	def new(conn, _) do
		render(conn, "new.html")
	end

	def create(conn, %{"user" => %{"name" => "", "password" => ""}}) do
		conn
		|> put_flash(:error, "Please fill in an email address and password")
		|> redirect(to: Routes.session_path(conn, :new))
	end

	def create(conn, %{"user" => %{"name" => "admin", "password" => "password"}}) do
				conn
				|> put_flash(:info, "welcome_back")
				|> put_session(:user_id, 1)
				|> configure_session(renew: true)
				|> redirect(to: "/")	
	end

	def delete(conn, _) do
		conn
		|> configure_session(drop: true)
		|> put_flash(:info,  "Signed Out")
		|> redirect(to: "/")
	end
end