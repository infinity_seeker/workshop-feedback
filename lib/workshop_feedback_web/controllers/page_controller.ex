defmodule WorkshopFeedbackWeb.PageController do
  use WorkshopFeedbackWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
